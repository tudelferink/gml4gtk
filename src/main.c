
/*
 *  Copyright t lefering
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

/* needed to get basename() from string.h */
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <cairo.h>
#include <cairo-svg.h>

#include "splay-tree.h"
#include "main.h"
#include "hier.h"
#include "uniqnode.h"
#include "uniqstr.h"
#include "bubbling.h"
#include "sugi.h"
#include "pos.h"
#include "gml_scanner.h"
#include "gml_parser.h"

/* Linux defaults */

/* name of font to use, example "Sans" */
#define DEFAULT_FONTNAME "Sans"

/* name of slant to use, example "Italic", "Oblique" or "Roman" */
#define DEFAULT_FONTSLANT "ROMAN"

/* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
#define DEFAULT_FONTWEIGHT "Light"

/* name of condensed to use, example "Semi-Condensed", "Condensed" */
#define DEFAULT_FONTCONDENSED "Condensed"

/* font size to use, example "10", "18", "20" etc. */
#define DEFAULT_FONTSIZE "10"

/* edge line thickness double value for example 0.5, 1.0, 1.5 */
#define DEFAULT_EDGE_THICKNESS 1.0

/* pixels in padding for gtk_box_pack try values 0, 1, 5, 20 */
#define PACKPADDING 0

/* window initial (x,y) size in pixels */
#define TOP_LEVEL_WINDOW_XSIZE 600
#define TOP_LEVEL_WINDOW_YSIZE 500

/* x,y spacing */
static int xspacing = 12;
static int yspacing = 24;

/* last open/save dir */
static char *lastopendir = NULL;
static char *lastsavedir = NULL;

/* the graph data */
static struct gml_graph *maingraph = NULL;

/* debug output to stdout */
static int option_gdebug = 0;	/* 0,1,2 */

/* draw edgelines as soft splines */
static int option_splines = 1;

/* exposed size of draw area */
static int drawing_area_xsize = 0;
static int drawing_area_ysize = 0;

/* set if there is data to draw */
static int validdata = 0;

/* r/g/b of drawing alt color "grey82" */
static int altr = 0xd1;
static int altg = 0xd1;
static int altb = 0xd1;

/* background r/g/b of drawing */
static int bgcr = 0xff;
static int bgcg = 0xff;
static int bgcb = 0xff;

/* zoom scaling factor changed by zoom slider */
static double zfactor = 1.0;

/* x offset changed by x slider */
static int vxmin = 0;

/* y offset changed by y slider */
static int vymin = 0;

/* top level window also used in maingtk2.c */
static GtkWidget *mainwindow1 = (GtkWidget *) 0;

/* where to draw */
static GtkWidget *drawingarea1 = (GtkWidget *) 0;

/* status line gtk buffers */
static GtkTextBuffer *entry1buffer = NULL;
static char charentry1buffer[80];

/* if set, draw dummy nodes */
static int drawdummy = 1;

/* sliders */
#if GTK_HAVE_API_VERSION_2 == 1
static GtkObject *adjvscale1 = NULL;
static GtkObject *adjvscale2 = NULL;
static GtkObject *adjhscale1 = NULL;
#endif

#if GTK_HAVE_API_VERSION_3 == 1
static GtkAdjustment *adjvscale1 = NULL;
static GtkAdjustment *adjvscale2 = NULL;
static GtkAdjustment *adjhscale1 = NULL;
#endif

/* forward decl. */
static void top_level_window_main_quit (void);
static void on_top_level_window_open1_activate (GtkMenuItem * menuitem,
						gpointer user_data);
static void on_top_level_window_dot1_activate (GtkMenuItem * menuitem,
					       gpointer user_data);
static void on_top_level_window_svg1_activate (GtkMenuItem * menuitem,
					       gpointer user_data);
static void on_top_level_window_quit1_activate (GtkMenuItem * menuitem,
						gpointer user_data);
static void check1_toggle (GtkWidget * widget, gpointer window);
static void dummy1_toggle (GtkWidget * widget, gpointer window);

static void
on_top_level_window_drawingarea1_expose_event_edges (cairo_t * crp);
static void
on_top_level_window_drawingarea1_expose_event_nodes (cairo_t * crp);

/* left slider */
static void on_vscale1_changed (GtkAdjustment * adj);

/* right slider */
static void on_vscale2_changed (GtkAdjustment * adj);

/* bottom slider */
static void on_hscale1_changed (GtkAdjustment * adj);

#if GTK_HAVE_API_VERSION_2 == 1
/* redraw drawing area */
static gboolean on_top_level_window_drawingarea1_expose_event (GtkWidget *
							       widget,
							       GdkEventExpose
							       * event,
							       gpointer
							       user_data);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
/* redraw drawing area */
static gboolean on_top_level_window_drawingarea1_draw_event (GtkWidget *
							     widget,
							     cairo_t * crdraw,
							     gpointer
							     user_data);
#endif

/* fit drawing in window */
static void dofit (void);

static void finalxy (void);

static void static_maingtk_textsizes (void);

static void update_status_text (char *text);

int
main (int argc, char *argv[])
{
  int status = 0;
  char *s;
  GtkWidget *vbox1;
  GtkWidget *menubar1;
  GtkWidget *menuitem1;
  GtkWidget *menuitem1_menu;
  GtkWidget *open1;
  GtkWidget *dot1;
  GtkWidget *svg1;
  GtkWidget *quit1;
  GtkWidget *hbox1;
  GtkWidget *vscale1;
  GtkWidget *vscale2;
  GtkWidget *hscale1;
  GtkWidget *hbox2;
  GtkWidget *check1;
  GtkWidget *dummy1;
  GtkWidget *entry1;

  /* get the home dir */
  s = getenv ("HOME");
  if (s)
    {
      lastopendir = strdup (s);
      lastsavedir = strdup (s);
    }
  else
    {
      /* there is no home dir set in env */
    }


#if !GLIB_CHECK_VERSION (2, 36, 0)
  /* for GDBus */
  g_type_init ();
#endif

  /*
   *    gtk_init (&argc, &argv); in gkt2, gtk3 and gtk_init() in gtk4
   *
   * calls the function gtk_init(gint *argc, gchar ***argv) which will be called in all GTK applications. 
   * This sets up a few things for us such as the default visual and color map and then proceeds to call 
   * gdk_init(gint *argc, gchar ***argv). This function initializes the library for use, sets up default 
   * signal handlers, and checks the arguments passed to your application on the command line, 
   * looking for one of the following:
   *
   *    * --gtk-module
   *    * --g-fatal-warnings
   *    * --gtk-debug
   *    * --gtk-no-debug
   *    * --gdk-debug
   *    * --gdk-no-debug
   *    * --display
   *    * --sync
   *    * --no-xshm
   *    * --name
   *    * --class
   *
   * It removes these from the argument list, leaving anything it does not recognize for your application 
   * to parse or ignore. This creates a set of standard arguments accepted by all GTK applications.
   *
   */

  /* do gtk init, gtk will grab the gtk specific options on command line */
#if GTK_HAVE_API_VERSION_4 == 1
  (void) gtk_init ();
#else
  /* gtk 2, 3 */
  status = gtk_init_check (&argc, &argv);

  if (status == FALSE)
    {
      /* this does happen with sudo strace ./mooigraph for some reason */
      printf ("%s is in console mode and need in graphical mode to run\n",
	      argv[0]);
      fflush (stdout);
      return (0);
    }
#endif

  /* top level outer window */
  mainwindow1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  /* make sure to exit oke. */
  g_signal_connect (G_OBJECT (mainwindow1), "destroy",
		    G_CALLBACK (top_level_window_main_quit), NULL);

  /* needed for the cairo drawing */
  gtk_widget_set_app_paintable (mainwindow1, TRUE);

  /* use package string program name as set by configure in config.h */
  gtk_window_set_title (GTK_WINDOW (mainwindow1), PACKAGE_STRING);

  /* pre-set some size */
  gtk_window_set_default_size (GTK_WINDOW (mainwindow1),
			       TOP_LEVEL_WINDOW_XSIZE,
			       TOP_LEVEL_WINDOW_YSIZE);

  /* --- */

  /* vbox1 is a menu bar */
#if GTK_HAVE_API_VERSION_2 == 1
  vbox1 = gtk_vbox_new ( /* homogeneous */ FALSE, /* spacing */ 0);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (mainwindow1), vbox1);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  vbox1 = gtk_box_new (GTK_ORIENTATION_VERTICAL, /* spacing */ 0);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (mainwindow1), vbox1);
#endif

  /* --- */

  /* menu bar in vbox1 */
  menubar1 = gtk_menu_bar_new ();
  gtk_widget_show (menubar1);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ menubar1,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* --- */

  /* menu items in menu bar in vbox1 */
  menuitem1 = gtk_menu_item_new_with_mnemonic ("File");
  gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
  gtk_widget_show (menuitem1);

  /* --- */

  /* 'file' sub menu in menu items in menu bar in vbox1 */
  menuitem1_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);

  /* --- */

  /* 'open' in 'file' sub menu in menu items in menu bar in vbox1 */
  open1 = gtk_menu_item_new_with_mnemonic ("Open GML");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), open1);
  gtk_widget_show (open1);

  /* run this routine when selected 'open' in 'file' menu */
  g_signal_connect (G_OBJECT (open1), "activate",
		    G_CALLBACK (on_top_level_window_open1_activate), NULL);

  dot1 = gtk_menu_item_new_with_mnemonic ("Save as DOT");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), dot1);
  gtk_widget_show (dot1);

  /* run this routine when selected 'open' in 'file' menu */
  g_signal_connect (G_OBJECT (dot1), "activate",
		    G_CALLBACK (on_top_level_window_dot1_activate), NULL);

  svg1 = gtk_menu_item_new_with_mnemonic ("Save as SVG");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), svg1);
  gtk_widget_show (svg1);

  /* run this routine when selected 'open' in 'file' menu */
  g_signal_connect (G_OBJECT (svg1), "activate",
		    G_CALLBACK (on_top_level_window_svg1_activate), NULL);

  /* 'quit' in 'file' sub menu in menu items in menu bar in vbox1 */
  quit1 = gtk_menu_item_new_with_mnemonic ("Quit");
  gtk_container_add (GTK_CONTAINER (menuitem1_menu), quit1);
  gtk_widget_show (quit1);

  /* run this routine when selected 'quit' in 'file' menu */
  g_signal_connect (G_OBJECT (quit1), "activate",
		    G_CALLBACK (on_top_level_window_quit1_activate), NULL);

  /*
   * in hbox1
   * left zoom slider
   * drawing area
   * right y slider
   * below x slider
   */

  /* add next area to the vbox1 */
#if GTK_HAVE_API_VERSION_2 == 1
  hbox1 = gtk_hbox_new ( /* homogeneous */ FALSE, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox1,
		      /* expand */ TRUE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox1);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  hbox1 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox1,
		      /* expand */ TRUE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox1);
#endif

  /* vertical slider in hbox1 for the zoom factor 50% is 1:1 */
  adjvscale1 = gtk_adjustment_new (50, 0, 100, 0, 0, 0);

#if GTK_HAVE_API_VERSION_2 == 1
  vscale1 = gtk_vscale_new (GTK_ADJUSTMENT (adjvscale1));
  g_signal_connect (G_OBJECT (adjvscale1), "value_changed",
		    GTK_SIGNAL_FUNC (on_vscale1_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ vscale1,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (vscale1), FALSE);
  gtk_widget_show (vscale1);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  vscale1 =
    gtk_scale_new (GTK_ORIENTATION_VERTICAL, GTK_ADJUSTMENT (adjvscale1));
  g_signal_connect (G_OBJECT (adjvscale1), "value_changed",
		    G_CALLBACK (on_vscale1_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ vscale1,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (vscale1), FALSE);
  gtk_widget_show (vscale1);
#endif

  /* where to draw in hbox1 */
  drawingarea1 = gtk_drawing_area_new ();
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ drawingarea1,
		      /* expand */ TRUE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (drawingarea1);

#if GTK_HAVE_API_VERSION_2 == 1
  g_signal_connect (G_OBJECT (drawingarea1), "expose_event",
		    G_CALLBACK
		    (on_top_level_window_drawingarea1_expose_event), NULL);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  g_signal_connect (G_OBJECT (drawingarea1), "draw",
		    G_CALLBACK (on_top_level_window_drawingarea1_draw_event),
		    NULL);
#endif

  /* vertical slider in hbox1 for the y position range 0...100% of full image size */
  adjvscale2 = gtk_adjustment_new (0, 0, 100, 0, 0, 0);

#if GTK_HAVE_API_VERSION_2 == 1
  vscale2 = gtk_vscale_new (GTK_ADJUSTMENT (adjvscale2));
  g_signal_connect (G_OBJECT (adjvscale2), "value_changed",
		    GTK_SIGNAL_FUNC (on_vscale2_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ vscale2,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (vscale2), FALSE);
  gtk_widget_show (vscale2);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  vscale2 =
    gtk_scale_new (GTK_ORIENTATION_VERTICAL, GTK_ADJUSTMENT (adjvscale2));
  g_signal_connect (G_OBJECT (adjvscale2), "value_changed",
		    G_CALLBACK (on_vscale2_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox1), /* child */ vscale2,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (vscale2), FALSE);
  gtk_widget_show (vscale2);
#endif

  /* horizontal scroller 0..100% of drawing size */
  adjhscale1 = gtk_adjustment_new (0, 0, 100, 0, 0, 0);

#if GTK_HAVE_API_VERSION_2 == 1
  hscale1 = gtk_hscale_new (GTK_ADJUSTMENT (adjhscale1));
  g_signal_connect (G_OBJECT (adjhscale1), "value_changed",
		    GTK_SIGNAL_FUNC (on_hscale1_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hscale1,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (hscale1), FALSE);
  gtk_widget_show (hscale1);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  hscale1 =
    gtk_scale_new (GTK_ORIENTATION_HORIZONTAL, GTK_ADJUSTMENT (adjhscale1));
  g_signal_connect (G_OBJECT (adjhscale1), "value_changed",
		    G_CALLBACK (on_hscale1_changed), NULL);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hscale1,
		      /* expand */ FALSE, /* fill */ TRUE,	/* padding */
		      PACKPADDING);
  gtk_scale_set_draw_value (GTK_SCALE (hscale1), FALSE);
  gtk_widget_show (hscale1);
#endif

  /* --- */

  /* add next area to the vbox1 */
#if GTK_HAVE_API_VERSION_2 == 1
  hbox2 = gtk_hbox_new ( /* homogeneous */ FALSE, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox2,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox2);
#endif

#if GTK_HAVE_API_VERSION_3 == 1
  hbox2 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, /* spacing */ 0);
  gtk_box_pack_start ( /* box */ GTK_BOX (vbox1), /* child */ hbox2,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (hbox2);
#endif

  /* --- */

  /* draw spline edges or normal */
  check1 = gtk_check_button_new_with_label ("splines");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ check1,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* */
  if (option_splines)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check1), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check1), FALSE);
    }

  g_signal_connect (G_OBJECT (check1), "clicked", G_CALLBACK (check1_toggle),
		    (gpointer) mainwindow1);
  gtk_widget_show (check1);

  /* draw dummy nodes */
  dummy1 = gtk_check_button_new_with_label ("dummy's");
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ dummy1,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);

  /* */
  if (drawdummy)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dummy1), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dummy1), FALSE);
    }

  g_signal_connect (G_OBJECT (dummy1), "clicked", G_CALLBACK (dummy1_toggle),
		    (gpointer) mainwindow1);
  gtk_widget_show (dummy1);


  /* */
  entry1 = gtk_text_view_new ();
  entry1buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry1));
  gtk_text_buffer_set_text (entry1buffer,
			    "gml4gtk is GNU GPL free software free to copy and improve",
			    -1);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (entry1), FALSE);
  gtk_box_pack_start ( /* box */ GTK_BOX (hbox2), /* child */ entry1,
		      /* expand */ FALSE, /* fill */ FALSE,	/* padding */
		      PACKPADDING);
  gtk_widget_show (entry1);


  /*
   * here additional gtk elements
   */

  /* put on screen */
  gtk_widget_show (mainwindow1);

  /* run the gui */
  gtk_main ();

  /* clean memory */
  if (maingraph)
    {
      clear_uniqstr ();
      clear_uniqnode ();
      clear_rawnodelist (maingraph);
      clear_rawedgelist (maingraph);
      clear_nodelist (maingraph);
      clear_edgelist (maingraph);
      clear_bubbling (maingraph);
      free (maingraph);
      maingraph = NULL;
    }

  return (0);
}

/* finally stop the gui */
static void
top_level_window_main_quit (void)
{

  /* run the gtk internal routine to stop gtk_main() which is a for(){} loop */
  gtk_main_quit ();

  return;
}

/* 'open' in 'file' menu activated - sub menu in menu items in menu bar in vbox1 */
static void
on_top_level_window_open1_activate (GtkMenuItem * menuitem,
				    gpointer user_data)
{
  GtkWidget *edialog = (GtkWidget *) 0;
  GtkWidget *pdialog = (GtkWidget *) 0;
  GtkWidget *dialog = (GtkWidget *) 0;
  char *file_chooser_filename = (char *) 0;
  char *file_chooser_dir = (char *) 0;
  GtkFileChooser *chooser = NULL;
  char *inputfilename = (char *) 0;
  char *baseinputfilename = (char *) 0;
  char *baseinputfilename2 = (char *) 0;
  FILE *f = NULL;

  if (menuitem)
    {
    }
  if (user_data)
    {
    }

#if GTK_HAVE_API_VERSION_2 == 1

  /* see gimp source code howto */
  dialog = gtk_file_chooser_dialog_new ("Select GML Graph File", 0,	/* parent_window */
					GTK_FILE_CHOOSER_ACTION_OPEN,
					GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					NULL);

#endif

#if GTK_HAVE_API_VERSION_3 == 1

  /* see gimp source code howto */
  dialog = gtk_file_chooser_dialog_new ("Select GML Graph File", GTK_WINDOW (mainwindow1)	/* parent_window */
					,
					GTK_FILE_CHOOSER_ACTION_OPEN,
					"Cancel", GTK_RESPONSE_CANCEL, "Open",
					GTK_RESPONSE_ACCEPT, NULL);

#endif

  chooser = GTK_FILE_CHOOSER (dialog);

  /* use same dir if opened in earlier dir */
  if (lastopendir)
    {
      gtk_file_chooser_set_current_folder (chooser, lastopendir);
    }

  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (mainwindow1));

  /* run the window to select a input file */
  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
      /* open button */
      file_chooser_filename =
	(char *) gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
      file_chooser_dir =
	(char *) gtk_file_chooser_get_current_folder (chooser);
    }
  else
    {
      /* cancel button */
      (void) gtk_widget_destroy (dialog);
      return;
    }

  /* */
  (void) gtk_widget_destroy (dialog);

  /* update last-used-dir */
  if (file_chooser_dir)
    {
      if (lastopendir)
	{
	  (void) g_free (lastopendir);
	}
      lastopendir = strdup (file_chooser_dir);
      (void) g_free (file_chooser_dir);
    }

  /* copy the input filename from gtk */
  if (file_chooser_filename)
    {
      inputfilename = strdup (file_chooser_filename);
      (void) g_free (file_chooser_filename);
    }
  else
    {
      return;
    }


  /* set filename in window */
  baseinputfilename2 = strdup (basename (inputfilename));

  /* open file to parse */
  errno = 0;
  f = fopen (inputfilename, "r");

  if (f == NULL)
    {
      edialog = gtk_message_dialog_new (GTK_WINDOW (mainwindow1),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_CLOSE,
					"Cannot open file %s for reading (%s)",
					inputfilename, g_strerror (errno));
      gtk_dialog_run (GTK_DIALOG (edialog));
      gtk_widget_destroy (edialog);
      free (inputfilename);
      free (baseinputfilename2);
      /* data is unchanged, so keep validdata status */
      return;
    }

  /* no draw data */
  validdata = 0;

  if (maingraph)
    {
      clear_uniqstr ();
      clear_uniqnode ();
      clear_rawnodelist (maingraph);
      clear_rawedgelist (maingraph);
      clear_nodelist (maingraph);
      clear_edgelist (maingraph);
      clear_bubbling (maingraph);
      free (maingraph);
      maingraph = NULL;
    }

  maingraph = calloc (1, sizeof (struct gml_graph));

  /* parse the gml data */
  if (gmlparse (maingraph, f, baseinputfilename2))
    {
      /* parse error */
      if (strlen (parsermessage) == 0)
	{
	  strcpy (parsermessage, "no parser message");
	}
      pdialog = gtk_message_dialog_new (GTK_WINDOW (mainwindow1),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_CLOSE,
					"%s", parsermessage);
      gtk_dialog_run (GTK_DIALOG (pdialog));
      gtk_widget_destroy (pdialog);
      free (inputfilename);
      free (baseinputfilename2);
      fclose (f);
      /* data is invalid at this point */
      validdata = 0;
      /* re draw screen */
      gtk_widget_queue_draw (drawingarea1);
      return;
    }

  fclose (f);

  baseinputfilename = uniqstr (basename (inputfilename));
  gtk_window_set_title (GTK_WINDOW (mainwindow1), baseinputfilename);

  /* check for empty graph here */
  if (maingraph->nodelist)
    {

      /* update status text */
      update_status_text ("Wait...Calculating Layout");

      /* split edges with label into node->label->node */
      edgelabels (maingraph);

      /* change cycles in the graph */
      uncycle (maingraph);

      /* change edge directions downwards */
      edgesdownwards (maingraph);

      /* set y level of all nodes */
      ylevels (maingraph);

      /* check length of edges */
      edgelen (maingraph);

      /* try to find shorter edges */
      shorteredges (maingraph);

      /* doublespace the vertical levels */
      doublespacey (maingraph);

      /* split longer edges */
      splitedges (maingraph);

      /* create level node count data */
      nodecounts (maingraph);

      /* run barycenter using defaults */
      reduce_crossings2 (maingraph, 0, 0);

      /* calculate size of text area */
      static_maingtk_textsizes ();

      /* run priority algorithm */
      improve_positions (maingraph);

      /* final (x,y) positioning of nodes/edges */
      finalxy ();

      /* update status text */
      update_status_text (NULL);

      /* set sliders to defaults */
      zfactor = 1.0;
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale1), 50);

      vxmin = 0;
      vymin = 0;
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), 0);
      gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), 0);

      /* filename is not saved */
      (void) g_free (inputfilename);
      free (baseinputfilename2);

      /* fit drawing in window */
      dofit ();

      validdata = 1;
    }
  else
    {
      /* filename is not saved */
      (void) g_free (inputfilename);
      free (baseinputfilename2);
      /* update status text */
      update_status_text ("Empty graph...No Nodes");
      validdata = 0;
    }


  /* re draw screen */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* save as simple dot graph, without tweaks, does not always work. */
static void
on_top_level_window_dot1_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  GtkWidget *dialog = (GtkWidget *) 0;
  GtkWidget *edialog = (GtkWidget *) 0;
  char *file_chooser_filename = (char *) 0;
  GtkFileChooser *chooser = NULL;
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
  gint res = 0;
  char *dotfilename = NULL;
  FILE *fstream = NULL;

  if (validdata == 0)
    {
      return;
    }

  if (menuitem)
    {
    }
  if (user_data)
    {
    }

  dialog = gtk_file_chooser_dialog_new ("Save As DOT graph",
					/* parent_window */ NULL,
					action, "_Cancel",
					GTK_RESPONSE_CANCEL, "_Save",
					GTK_RESPONSE_ACCEPT, NULL);

  chooser = GTK_FILE_CHOOSER (dialog);

  /* change to last used dir if any */
  if (lastsavedir)
    {
      gtk_file_chooser_set_current_folder (chooser, lastsavedir);
    }

  /* ask to override existing */
  gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

  /* get the filename */
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (mainwindow1));

  res = gtk_dialog_run (GTK_DIALOG (dialog));

  if (res == GTK_RESPONSE_ACCEPT)
    {
      file_chooser_filename = gtk_file_chooser_get_filename (chooser);
      lastsavedir = gtk_file_chooser_get_current_folder (chooser);
    }
  else
    {
      /* cancel button */
      (void) gtk_widget_destroy (dialog);
      return;
    }

  /* */
  (void) gtk_widget_destroy (dialog);

  /* */
  if (file_chooser_filename)
    {
      dotfilename = strdup (file_chooser_filename);
      /* */
      (void) g_free (file_chooser_filename);
    }
  else
    {
      /* no filename */
      return;
    }

  errno = 0;
  fstream = fopen (dotfilename, "wb");

  /* check if open */
  if (fstream == NULL)
    {
      edialog = gtk_message_dialog_new (GTK_WINDOW (mainwindow1),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_CLOSE,
					"Cannot open file %s for writing (%s)",
					dotfilename, g_strerror (errno));
      gtk_dialog_run (GTK_DIALOG (edialog));
      gtk_widget_destroy (edialog);
      free (dotfilename);
      return;
    }

  /* write the dot graph */
  raw2dot (maingraph, fstream);

  fclose (fstream);
  free (dotfilename);

  return;
}

/* save as gtk svg */
static void
on_top_level_window_svg1_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  GtkWidget *dialog = (GtkWidget *) 0;
  GtkWidget *edialog = (GtkWidget *) 0;
  char *file_chooser_filename = (char *) 0;
  GtkFileChooser *chooser = NULL;
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
  gint res = 0;
  char *svgfilename = NULL;
  FILE *fstream = NULL;
  int mymaxx = 0;
  int mymaxy = 0;
  int saved_vxmin = 0;
  int saved_vymin = 0;
  cairo_surface_t *surface = NULL;
  cairo_t *crp = NULL;

  if (validdata == 0)
    {
      return;
    }

  if (menuitem)
    {
    }
  if (user_data)
    {
    }

  dialog = gtk_file_chooser_dialog_new ("Save As SVG Image",
					/* parent_window */ NULL,
					action, "_Cancel",
					GTK_RESPONSE_CANCEL, "_Save",
					GTK_RESPONSE_ACCEPT, NULL);

  chooser = GTK_FILE_CHOOSER (dialog);

  /* change to last used dir if any */
  if (lastsavedir)
    {
      gtk_file_chooser_set_current_folder (chooser, lastsavedir);
    }

  /* ask to override existing */
  gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

  /* get the filename */
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (mainwindow1));

  res = gtk_dialog_run (GTK_DIALOG (dialog));

  if (res == GTK_RESPONSE_ACCEPT)
    {
      file_chooser_filename = gtk_file_chooser_get_filename (chooser);
      lastsavedir = gtk_file_chooser_get_current_folder (chooser);
    }
  else
    {
      /* cancel button */
      (void) gtk_widget_destroy (dialog);
      return;
    }

  /* */
  (void) gtk_widget_destroy (dialog);

  /* */
  if (file_chooser_filename)
    {
      svgfilename = strdup (file_chooser_filename);
      /* */
      (void) g_free (file_chooser_filename);
    }
  else
    {
      /* no filename */
      return;
    }

  errno = 0;
  fstream = fopen (svgfilename, "wb");

  /* check if open */
  if (fstream == NULL)
    {
      edialog = gtk_message_dialog_new (GTK_WINDOW (mainwindow1),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_CLOSE,
					"Cannot open file %s for writing (%s)",
					svgfilename, g_strerror (errno));
      gtk_dialog_run (GTK_DIALOG (edialog));
      gtk_widget_destroy (edialog);
      free (svgfilename);
      return;
    }

  /* write the svg image data */

  /* save current gui settings */
  saved_vxmin = vxmin;
  saved_vymin = vymin;

  /* output whole drawing with a small border, then edge line at end is in drawing */
  vxmin = 0;
  vymin = 0;
  mymaxx = (int) ((maxx + xspacing) * /* 1.0 */ zfactor);
  mymaxy = (int) ((maxy + yspacing) * /* 1.0 */ zfactor);

  surface = cairo_svg_surface_create (svgfilename, mymaxx, mymaxy);

  /* */
  crp = cairo_create (surface);

  /* fill drawing background with background color */
  cairo_set_source_rgb (crp, bgcr / 255.0, bgcg / 255.0, bgcb / 255.0);

  /* select whole drawing to fill wth background color */
  cairo_rectangle (crp, 0, 0, mymaxx, mymaxy);

  cairo_fill (crp);

  /* use zoom slider drawing scale */
  cairo_scale (crp, /*1.0 */ zfactor, /*1.0 */ zfactor);

  on_top_level_window_drawingarea1_expose_event_edges (crp);
  on_top_level_window_drawingarea1_expose_event_nodes (crp);

  cairo_destroy (crp);
  cairo_surface_destroy (surface);

  /* restore screen (x,y) min */
  vxmin = saved_vxmin;
  vymin = saved_vymin;

  fclose (fstream);
  free (svgfilename);

  return;
}

/* 'quit' in 'file' menu activated - sub menu in menu items in menu bar in vbox1 */
static void
on_top_level_window_quit1_activate (GtkMenuItem * menuitem,
				    gpointer user_data)
{
  if (menuitem)
    {
    }
  if (user_data)
    {
    }
  top_level_window_main_quit ();
  return;
}

/* left slider zoom factor */
static void
on_vscale1_changed (GtkAdjustment * adj)
{
  gdouble val0 = 0.0;
  int val1 = 0;

  if (adj)
    {
    }

  /* check if there is node data to draw */
  if (validdata == 0)
    {
      return;
    }

  val0 = gtk_adjustment_get_value (adj);
  val1 = (int) val0;
  zfactor = exp ((double) (3 * (val1 - 50)) / (double) 50);

  if (option_gdebug > 1)
    {
      printf ("%s(): zoomslider=%d zoomfactor=%f\n", "on_vscale1_changed",
	      val1, zfactor);
      fflush (stdout);
    }

  /* do a re-draw */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* right slider y offset 0...100% */
static void
on_vscale2_changed (GtkAdjustment * adj)
{
  gdouble val = 0.0;
  if (adj)
    {
    }

  /* check if there is node data to draw */
  if (validdata == 0)
    {
      return;
    }

  val = gtk_adjustment_get_value (adj);
  vymin = (int) ((val * maxy) / 100);

  if (option_gdebug > 1)
    {
      printf ("%s(): yslider=%d vymin=%d (maxy=%d)\n", "on_vscale2_changed",
	      (int) val, vymin, maxy);
      fflush (stdout);
    }

  /* do a re-draw */
  gtk_widget_queue_draw (drawingarea1);

  return;
}

/* bottom slider x offset 0...100% */
static void
on_hscale1_changed (GtkAdjustment * adj)
{
  gdouble val = 0.0;
  if (adj)
    {
    }

  /* check if there is node data to draw */
  if (validdata == 0)
    {
      return;
    }

  val = gtk_adjustment_get_value (adj);
  vxmin = (int) ((val * maxx) / 100);

  if (option_gdebug > 1)
    {
      printf ("%s(): xslider=%d vxmin=%d (maxx=%d)\n", "on_hscale1_changed",
	      (int) val, vxmin, maxx);
      fflush (stdout);
    }

  /* do a re-draw */
  gtk_widget_queue_draw (drawingarea1);

  return;
}


/* draw all nodes */
static void
on_top_level_window_drawingarea1_expose_event_nodes (cairo_t * crp)
{
  int xo = 0;
  int yo = 0;
  double dfs = 0.0;
  int fs = 0;
  int r = 0;
  int g = 0;
  int b = 0;
  PangoLayout *layout = NULL;
  PangoFontDescription *desc = NULL;
  char buf[128];
  char *s = NULL;
  /* name of font to use, example "Sans" */
  const char *default_fontname = DEFAULT_FONTNAME;
  /* name of slant to use, example "Italic", "Oblique", "Roman" */
  const char *default_fontslant = DEFAULT_FONTSLANT;
  /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
  const char *default_fontweight = DEFAULT_FONTWEIGHT;
  /* name of condensed to use, example "Semi-Condensed", "Condensed" */
  const char *default_fontcondensed = DEFAULT_FONTCONDENSED;
  /* font size to use, example "10", "18", "20" etc. */
  const char *default_fontsize = DEFAULT_FONTSIZE;
  int x0 = 0;
  int y0 = 0;
  struct gml_nlist *nl;

  nl = maingraph->nodelist;

  while (nl)
    {
      /* only draw real nodes here or edgeabels */
      if (nl->node->dummy == 0)
	{
	  x0 = nl->node->finx - vxmin;
	  y0 = nl->node->finy - vymin;

	  /* fillcolor of node white default or color */
	  r = (nl->node->ncolor & 0x00ff0000) >> 16;
	  g = (nl->node->ncolor & 0x0000ff00) >> 8;
	  b = (nl->node->ncolor & 0x000000ff);

	  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
	  cairo_rectangle (crp, x0, y0, nl->node->bbx, nl->node->bby);
	  cairo_fill (crp);
	  cairo_stroke (crp);

	  /* outline color black for node, edgelabel node has no outline */
	  if (nl->node->elabel == 0)
	    {
	      r = 0;
	      g = 0;
	      b = 0;
	      cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);
	      cairo_rectangle (crp, x0, y0, nl->node->bbx, nl->node->bby);
	      cairo_stroke (crp);
	    }

	  /* calculate the scaled font size */
	  dfs = (zfactor * atoi (default_fontsize));

	  /* somewhat rounded version */
	  fs = (int) dfs;

	  /* too small to draw readable text then skip the following text drawing area */
	  if (fs < 4)
	    {
	      nl = nl->next;
	      continue;
	    }

	  /* black text */
	  r = 0;
	  g = 0;
	  b = 0;
	  /* draw in text color of node */
	  cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

	  xo = 2;
	  yo = 2;
	  /* set start position of text */
	  cairo_move_to (crp, nl->node->finx - vxmin + xo,
			 nl->node->finy - vymin + yo);

	  layout = pango_cairo_create_layout (crp);

	  /* set the text to draw which is 0 terminated */
	  pango_layout_set_text (layout, nl->node->nlabel, -1);

	  /* set font parameters */

	  /* create the fontname description */
	  memset (buf, 0, 128);

	  default_fontname = DEFAULT_FONTNAME;

	  /* check if node has a specified font slant */
	  default_fontslant = DEFAULT_FONTSLANT;

	  /* check if node has a specified font weight */
	  default_fontweight = DEFAULT_FONTWEIGHT;

	  /* check if node has a specified font size */
	  default_fontsize = DEFAULT_FONTSIZE;

	  /* create the font name string */
	  snprintf (buf, (128 - 1), "%s %s %s %s %s", default_fontname,
		    default_fontslant, default_fontweight,
		    default_fontcondensed, default_fontsize);

	  /* copy string buffer */
	  s = uniqstr (buf);

	  /* */
	  desc = pango_font_description_from_string (s);

	  /* */
	  pango_layout_set_font_description (layout, desc);

	  /* */
	  pango_font_description_free (desc);

	  /* */
	  pango_cairo_update_layout (crp, layout);

	  /* draw the text */
	  pango_cairo_show_layout (crp, layout);

	  /* */
	  cairo_stroke (crp);

	  g_object_unref (G_OBJECT (layout));
	}

      nl = nl->next;
    }

  return;
}

/* draw arrow, option to allow multiple arrow types todo */
static void
drarrow (cairo_t * crp, int start_x, int start_y, int end_x, int end_y)
{
  double angle = 0.0;
  double arrow_lenght = 8.0;
  double arrow_degrees = ( /* degrees */ 30 * (M_PI / 180));
  double x1 = 0.0;
  double y1 = 0.0;
  double x2 = 0.0;
  double y2 = 0.0;

  if (0)
    {
      printf ("%s(): arrow at (%d,%d) coming from (%d,%d)\n", __FUNCTION__,
	      end_x, end_y, start_x, start_y);
    }

  /* gtk has a gtk_render_arrow() function as alternative to use here */
  angle = atan2 (end_y - start_y, end_x - start_x) + M_PI;

  x1 = end_x + arrow_lenght * cos (angle - arrow_degrees);
  y1 = end_y + arrow_lenght * sin (angle - arrow_degrees);
  x2 = end_x + arrow_lenght * cos (angle + arrow_degrees);
  y2 = end_y + arrow_lenght * sin (angle + arrow_degrees);

  /* set start position of arrow part 1 */
  cairo_move_to (crp, end_x, end_y);
  cairo_line_to (crp, x1, y1);

  /* set start position of arrow part 2 */
  cairo_move_to (crp, end_x, end_y);
  cairo_line_to (crp, x2, y2);

  /* make a triangle */
  cairo_line_to (crp, x1, y1);

  return;
}

/* nr. of pixels hor. edge goes downwards */
#define HEDGE_DY 10

/* draw all edges */
static void
on_top_level_window_drawingarea1_expose_event_edges (cairo_t * crp)
{
  int r = 0;
  int g = 0;
  int b = 0;
  int fnx1 = 0;
  int fny1 = 0;
  int tnx1 = 0;
  int tny1 = 0;
  struct gml_elist *el = NULL;
  struct gml_edge *edge = NULL;
  struct gml_node *sn = NULL;
  struct gml_node *tn = NULL;
  int ecolor = 0;
  int dx15 = 0;

  el = maingraph->edgelist;

  while (el)
    {
      edge = el->edge;

      sn = edge->snode;		/* from-node */
      tn = edge->tnode;		/* to-node */

      ecolor = edge->ecolor;	/* edge line color */

      /* thickness of edge line */
      cairo_set_line_width (crp, DEFAULT_EDGE_THICKNESS);

      /* black or colored line */
      r = (ecolor & 0x00ff0000) >> 16;
      g = (ecolor & 0x0000ff00) >> 8;
      b = (ecolor & 0x000000ff);

      fnx1 = 0;
      fny1 = 0;
      tnx1 = 0;
      tny1 = 0;

      /* draw in line color of edge */
      cairo_set_source_rgb (crp, r / 255.0, g / 255.0, b / 255.0);

      if (edge->hedge)
	{
	  /* horizontal edge has original endpoints */

	  /* center of from/to nodes */
	  fnx1 = (sn->finx + sn->bbx / 2) - vxmin;
	  tnx1 = (tn->finx + tn->bbx / 2) - vxmin;

	  if (tnx1 > fnx1)
	    {
	      /* edge from left to right */

	      /* check if nodes are next to each other */
	      /* this should also check if to-node is a dummy or real node todo */
	      if ((sn->relx + 1) == tn->relx)
		{
		  /*  printf ("direct-lr\n");  */

		  fnx1 = (sn->finx + sn->bbx) - vxmin;
		  fny1 = (sn->finy + sn->bby / 2) - vymin;
		  cairo_move_to (crp, fnx1, fny1);

		  cairo_line_to (crp, (tn->finx) - vxmin,
				 (tn->finy + tn->bby / 2) - vymin);

		  /* add arrow */
		  if (edge->reversed == 0)
		    {
		      drarrow (crp, fnx1, fny1, (tn->finx) - vxmin,
			       (tn->finy + tn->bby / 2) - vymin);
		    }
		  else
		    {
		      drarrow (crp, (tn->finx) - vxmin,
			       (tn->finy + tn->bby / 2) - vymin, fnx1, fny1);
		    }

		}
	      else
		{


		  /* distance length of hor. line / 5 */
		  dx15 = (tnx1 - fnx1) / 5;

		  /* optional draw hor. edges in a different color */
		  if (0)
		    {
		      cairo_set_source_rgb (crp, 0 / 255.0, 0 / 255.0,
					    0xff / 255.0);
		    }

		  /* start line at center */
		  fnx1 = (sn->finx + sn->bbx / 2) - vxmin;
		  fny1 = (sn->finy + sn->bby) - vymin;
		  cairo_move_to (crp, fnx1, fny1);

		  if ((sn->finy + sn->bby) >= (tn->finy + tn->bby))
		    {
		      /* from node y is lower then target, put hor. line 10px lower */
		      cairo_line_to (crp, (fnx1 + dx15),
				     ((sn->finy + sn->bby) + HEDGE_DY) -
				     vymin);
		      cairo_line_to (crp, (fnx1 + dx15) + (3 * dx15),
				     ((sn->finy + sn->bby) + HEDGE_DY) -
				     vymin);
		      cairo_line_to (crp, (tn->finx + tn->bbx / 2) - vxmin,
				     (tn->finy + tn->bby) - vymin);

		      /* add arrow */
		      if (edge->reversed == 0)
			{
			  drarrow (crp, (fnx1 + dx15) + (3 * dx15),
				   ((sn->finy + sn->bby) + HEDGE_DY) -
				   vymin, (tn->finx + tn->bbx / 2) - vxmin,
				   (tn->finy + tn->bby) - vymin);
			}
		      else
			{
			  drarrow (crp, (fnx1 + dx15),
				   ((sn->finy + sn->bby) + HEDGE_DY) -
				   vymin, fnx1, fny1);
			}

		    }
		  else
		    {
		      /* from-node y is higher */
		      cairo_line_to (crp, (fnx1 + dx15),
				     (tn->finy + tn->bby) + HEDGE_DY - vymin);
		      cairo_line_to (crp, (fnx1 + dx15) + (3 * dx15),
				     ((tn->finy + tn->bby) + HEDGE_DY) -
				     vymin);
		      cairo_line_to (crp, (tn->finx + tn->bbx / 2) - vxmin,
				     (tn->finy + tn->bby) - vymin);

		      /* add arrow */
		      if (edge->reversed == 0)
			{
			  drarrow (crp, (fnx1 + dx15) + (3 * dx15),
				   ((tn->finy + tn->bby) + HEDGE_DY) -
				   vymin,
				   (tn->finx + tn->bbx / 2) - vxmin,
				   (tn->finy + tn->bby) - vymin);
			}
		      else
			{
			  drarrow (crp, (fnx1 + dx15),
				   (tn->finy + tn->bby) + HEDGE_DY -
				   vymin, fnx1, fny1);
			}

		    }

		}

	    }
	  else
	    {
	      /* edge from right to left fnx1>tnx1 */

	      if ((sn->relx) == (tn->relx + 1))
		{
		  /*  printf ("direct-rl\n");  */

		  cairo_move_to (crp, (sn->finx) - vxmin,
				 (sn->finy + sn->bby / 2) - vymin);
		  cairo_line_to (crp,
				 tn->finx + tn->bbx - vxmin,
				 tn->finy + tn->bby / 2 - vymin);
		  /* add arrow */
		  if (edge->reversed == 0)
		    {
		      drarrow (crp,
			       (sn->finx) - vxmin,
			       (sn->finy + sn->bby / 2) - vymin,
			       tn->finx + tn->bbx - vxmin,
			       tn->finy + tn->bby / 2 - vymin);
		    }
		  else
		    {
		      drarrow (crp, tn->finx + tn->bbx - vxmin,
			       tn->finy + tn->bby / 2 - vymin,
			       (sn->finx) - vxmin,
			       (sn->finy + sn->bby / 2) - vymin);
		    }

		}
	      else
		{

		  /* distance length of hor. line / 5 */
		  dx15 = (fnx1 - tnx1) / 5;
		  /* optional draw hor. edges in a different color */
		  if (0)
		    {
		      cairo_set_source_rgb (crp, 0 / 255.0,
					    0 / 255.0, 0xff / 255.0);
		    }

		  /* start line at center */
		  fnx1 = (sn->finx + sn->bbx / 2) - vxmin;
		  fny1 = (sn->finy + sn->bby) - vymin;
		  cairo_move_to (crp, fnx1, fny1);

		  if ((sn->finy + sn->bby) >= (tn->finy + tn->bby))
		    {
		      /* from node y is lower then target, put hor. line 10px lower */
		      cairo_line_to (crp, (fnx1 - dx15),
				     ((sn->finy + sn->bby) +
				      HEDGE_DY) - vymin);
		      cairo_line_to (crp,
				     (fnx1 - dx15) - (3 * dx15),
				     ((sn->finy + sn->bby) +
				      HEDGE_DY) - vymin);
		      cairo_line_to (crp,
				     (tn->finx + tn->bbx / 2) -
				     vxmin, (tn->finy + tn->bby) - vymin);

		      /* add arrow */
		      if (edge->reversed == 0)
			{
			  /* could be improved here todo */
			  drarrow (crp, (fnx1 - dx15) - (3 * dx15),
				   ((sn->finy + sn->bby) +
				    HEDGE_DY) - vymin,
				   (tn->finx + tn->bbx / 2) - vxmin,
				   (tn->finy + tn->bby) - vymin);
			}
		      else
			{
			  drarrow (crp, (fnx1 - dx15),
				   ((sn->finy + sn->bby) +
				    HEDGE_DY) - vymin, fnx1, fny1);
			}

		    }
		  else
		    {
		      /* from-node y is higher */
		      cairo_line_to (crp, (fnx1 - dx15),
				     (tn->finy + tn->bby) + HEDGE_DY - vymin);
		      cairo_line_to (crp,
				     (fnx1 - dx15) - (3 * dx15),
				     ((tn->finy + tn->bby) +
				      HEDGE_DY) - vymin);
		      cairo_line_to (crp,
				     (tn->finx + tn->bbx / 2) -
				     vxmin, (tn->finy + tn->bby) - vymin);
		      /* add arrow */
		      if (edge->reversed == 0)
			{
			  /* this could be improved, todo */
			  drarrow (crp, (fnx1 - dx15) - (3 * dx15),
				   ((sn->finy + sn->bby) +
				    HEDGE_DY) - vymin,
				   (tn->finx + tn->bbx / 2) - vxmin,
				   (tn->finy + tn->bby) - vymin);
			}
		      else
			{
			  drarrow (crp, (fnx1 - dx15),
				   (tn->finy + tn->bby) + HEDGE_DY -
				   vymin, fnx1, fny1);
			}

		    }

		}

	    }


	}
      else
	{

	  /* here if not a horizontal edge */
	  if (option_splines)
	    {
	      /* drawing spline lines */

	      if (sn->dummy == 0 && tn->dummy == 0)
		{
		  /* from real node to real node.
		   * because real nodes are interleaved with dummy nodes
		   * this should not happen.
		   */
		  printf ("%s(): shouldnothappen real->real\n", __FUNCTION__);

		  fnx1 = sn->finx + sn->bbx / 2 - vxmin;
		  fny1 = sn->finy + sn->bby - vymin;
		  cairo_move_to (crp, fnx1, fny1);
		  tnx1 = tn->finx + tn->bbx / 2 - vxmin;
		  tny1 = tn->finy - vymin;
		  cairo_line_to (crp, tnx1, tny1);

		  /* add arrow */
		  if (edge->reversed == 0)
		    {
		      drarrow (crp, fnx1, fny1, tnx1, tny1);
		    }
		  else
		    {
		      drarrow (crp, tnx1, tny1, fnx1, fny1);
		    }

		}
	      else if (sn->dummy && tn->dummy == 0)
		{
		  /* from dummy node to real node */
		  fnx1 = sn->finx - vxmin;
		  fny1 = sn->finy - vymin;
		  tnx1 = (tn->finx + (tn->bbx / 2)) - vxmin;
		  tny1 = tn->finy - vymin;

		  /* optional draw dummy node */
		  if (drawdummy)
		    {
		      cairo_set_source_rgb (crp, 255 / 255.0,
					    0 / 255.0, 0 / 255.0);
		      cairo_rectangle (crp, fnx1 - 1, fny1 - 1, 2, 2);
		      cairo_fill (crp);
		      /* draw in line color of edge */
		      cairo_set_source_rgb (crp, r / 255.0,
					    g / 255.0, b / 255.0);
		    }

		  if (0)
		    {		/* straight lines */
		      cairo_move_to (crp, fnx1, fny1);
		      cairo_line_to (crp, fnx1, fny1 + (sn->bby / 2));
		      cairo_line_to (crp, tnx1, tny1);
		    }

		  /* this can be done better, todo */
		  cairo_move_to (crp, fnx1, fny1);
		  cairo_curve_to (crp, fnx1, fny1, fnx1, fny1 + (sn->bby / 2),
				  tnx1, tny1);

		  /* add arrow */
		  if (edge->reversed == 0)
		    {
		      drarrow (crp, fnx1, fny1, tnx1, tny1);
		    }
		  else
		    {
		      /* no arrow */
		    }

		}
	      else if (sn->dummy == 0 && tn->dummy)
		{
		  /* from real node to dummy node */
		  fnx1 = (sn->finx + sn->bbx / 2) - vxmin;
		  fny1 = (sn->finy + sn->bby) - vymin;
		  tnx1 = tn->finx - vxmin;
		  tny1 = tn->finy - vymin;

		  /* optional draw dummy node */
		  if (drawdummy)
		    {
		      cairo_set_source_rgb (crp, 255 / 255.0,
					    0 / 255.0, 0 / 255.0);
		      cairo_rectangle (crp, tnx1 - 1, tny1 - 1, 2, 2);
		      cairo_fill (crp);
		      /* draw in line color of edge */
		      cairo_set_source_rgb (crp, r / 255.0,
					    g / 255.0, b / 255.0);
		    }

		  if (0)
		    {		/* straight */
		      cairo_move_to (crp, fnx1, fny1);
		      cairo_line_to (crp, tnx1, tny1 - (tn->bby / 2));
		      cairo_line_to (crp, tnx1, tny1);
		    }

		  /* this can been done better, todo */
		  cairo_move_to (crp, fnx1, fny1);
		  cairo_curve_to (crp, fnx1, fny1, tnx1, tny1 - (tn->bby / 2),
				  tnx1, tny1);

		  /* add arrow */
		  if (edge->reversed == 0)
		    {
		      /* no arrow */
		    }
		  else
		    {
		      drarrow (crp, tnx1, tny1, fnx1, fny1);
		    }

		}
	      else
		{
		  /* from dummy node to dummy node */
		  fnx1 = sn->finx - vxmin;
		  fny1 = sn->finy - vymin;
		  tnx1 = tn->finx - vxmin;
		  tny1 = tn->finy - vymin;

		  /* optional draw dummy node */
		  if (drawdummy)
		    {
		      cairo_set_source_rgb (crp, 255 / 255.0,
					    0 / 255.0, 0 / 255.0);
		      cairo_rectangle (crp, fnx1 - 1, fny1 - 1, 2, 2);
		      cairo_fill (crp);
		      cairo_rectangle (crp, tnx1 - 1, tny1 - 1, 2, 2);
		      cairo_fill (crp);
		      /* draw in line color of edge */
		      cairo_set_source_rgb (crp, r / 255.0,
					    g / 255.0, b / 255.0);
		    }

		  if (0)
		    {		/* straight */
		      cairo_move_to (crp, fnx1, fny1);
		      cairo_line_to (crp, fnx1, fny1 + (sn->bby / 2));
		      cairo_line_to (crp, tnx1, tny1 - (tn->bby / 2));
		      cairo_line_to (crp, tnx1, tny1);
		    }


		  /* this can be done better, todo */
		  if (0)
		    {		/* this does not work: */
		      cairo_move_to (crp, fnx1, fny1);
		      cairo_curve_to (crp, fnx1, fny1 + (sn->bby / 2), tnx1,
				      tny1 - (tn->bby / 2), tnx1, tny1);
		    }
		  else
		    {
		      /* fix attempt */
		      cairo_move_to (crp, fnx1, fny1);
		      cairo_line_to (crp, fnx1, fny1 + (sn->bby / 2));
		      cairo_curve_to (crp, tnx1, tny1 - (tn->bby / 4), tnx1,
				      tny1 - (tn->bby / 4), tnx1, tny1);
		    }

		  /* no real nodes, no arrows, optional draw arrows at dummy nodes, see tuxsee */

		}

	    }
	  else
	    {
	      /* drawing not-spline lines */

	      if (sn->dummy == 0 && tn->dummy == 0)
		{
		  /* from real node to real node.
		   * because real nodes are interleaved with dummy nodes
		   * this should not happen.
		   */
		  printf ("%s(): shouldnothappen real->real\n", __FUNCTION__);

		  /* from real node to real node */
		  fnx1 = sn->finx + sn->bbx / 2 - vxmin;
		  fny1 = sn->finy + sn->bby - vymin;
		  cairo_move_to (crp, fnx1, fny1);
		  tnx1 = tn->finx + tn->bbx / 2 - vxmin;
		  tny1 = tn->finy - vymin;
		  cairo_line_to (crp, tnx1, tny1);

		  /* add arrow */
		  if (edge->reversed == 0)
		    {
		      drarrow (crp, fnx1, fny1, tnx1, tny1);
		    }
		  else
		    {
		      drarrow (crp, tnx1, tny1, fnx1, fny1);
		    }

		}
	      else if (sn->dummy == 1 && tn->dummy == 0)
		{
		  /* from dummy node to real node */
		  fnx1 = sn->finx - vxmin;
		  fny1 = sn->finy - vymin;
		  tnx1 = (tn->finx + (tn->bbx / 2)) - vxmin;
		  tny1 = tn->finy - vymin;

		  /* optional draw dummy node */
		  if (drawdummy)
		    {
		      cairo_set_source_rgb (crp, 255 / 255.0,
					    0 / 255.0, 0 / 255.0);
		      cairo_rectangle (crp, fnx1 - 1, fny1 - 1, 2, 2);
		      cairo_fill (crp);
		      /* draw in line color of edge */
		      cairo_set_source_rgb (crp, r / 255.0,
					    g / 255.0, b / 255.0);
		    }

		  cairo_move_to (crp, fnx1, fny1);
		  cairo_line_to (crp, fnx1, fny1 + (sn->bby / 2));
		  cairo_line_to (crp, tnx1, tny1);

		  /* add arrow */
		  if (edge->reversed == 0)
		    {
		      drarrow (crp, fnx1, fny1, tnx1, tny1);
		    }
		  else
		    {
		      /* no arrow */
		    }

		}
	      else if (sn->dummy == 0 && tn->dummy == 1)
		{
		  /* from real node to dummy node */
		  fnx1 = (sn->finx + sn->bbx / 2) - vxmin;
		  fny1 = (sn->finy + sn->bby) - vymin;
		  tnx1 = tn->finx - vxmin;
		  tny1 = tn->finy - vymin;

		  /* optional draw dummy node */
		  if (drawdummy)
		    {
		      cairo_set_source_rgb (crp, 255 / 255.0,
					    0 / 255.0, 0 / 255.0);
		      cairo_rectangle (crp, tnx1 - 1, tny1 - 1, 2, 2);
		      cairo_fill (crp);
		      /* draw in line color of edge */
		      cairo_set_source_rgb (crp, r / 255.0,
					    g / 255.0, b / 255.0);
		    }

		  cairo_move_to (crp, fnx1, fny1);
		  cairo_line_to (crp, tnx1, tny1 - (tn->bby / 2));
		  cairo_line_to (crp, tnx1, tny1);

		  /* add arrow */
		  if (edge->reversed == 0)
		    {
		      /* no arrow */
		    }
		  else
		    {
		      drarrow (crp, tnx1, tny1, fnx1, fny1);
		    }

		}
	      else
		{
		  /* from dummy node to dummy node */
		  fnx1 = sn->finx - vxmin;
		  fny1 = sn->finy - vymin;
		  tnx1 = tn->finx - vxmin;
		  tny1 = tn->finy - vymin;

		  /* optional draw dummy node */
		  if (drawdummy)
		    {
		      cairo_set_source_rgb (crp, 255 / 255.0,
					    0 / 255.0, 0 / 255.0);
		      cairo_rectangle (crp, fnx1 - 1, fny1 - 1, 2, 2);
		      cairo_fill (crp);
		      cairo_rectangle (crp, tnx1 - 1, tny1 - 1, 2, 2);
		      cairo_fill (crp);
		      /* draw in line color of edge */
		      cairo_set_source_rgb (crp, r / 255.0,
					    g / 255.0, b / 255.0);
		    }

		  cairo_move_to (crp, fnx1, fny1);
		  cairo_line_to (crp, fnx1, fny1 + (sn->bby / 2));
		  cairo_line_to (crp, tnx1, tny1 - (tn->bby / 2));
		  cairo_line_to (crp, tnx1, tny1);

		  /* no arrow draw because there are no real nodes */
		}

	      /* put the lines on screen */
	      cairo_stroke (crp);
	    }

	}

      cairo_stroke (crp);
      el = el->next;
    }

  return;
}

#if GTK_HAVE_API_VERSION_2 == 1
/* redraw drawing area */
static gboolean
  on_top_level_window_drawingarea1_expose_event
  (GtkWidget * widget, GdkEventExpose * event, gpointer user_data)
{
  cairo_t *crdraw = NULL;
  gint w = 0;			/* xsize of drawing area */
  gint h = 0;			/* ysize of drawing area */
  int fit = 0;
  if (widget)
    {
    }
  if (event)
    {
    }
  if (user_data)
    {
    }

  /* only at first expose */
  if (drawing_area_xsize == 0 && drawing_area_ysize == 0)
    {
      fit = 1;
    }
  else
    {
      fit = 0;
    }

  if (fit)
    {
    }

  /* get cairo drawing context */
  crdraw = gdk_cairo_create (event->window);
  /* how large drawing area is */
  (void) gdk_drawable_get_size (event->window, &w, &h);
  if (option_gdebug > 1 || 0)
    {
      printf
	("%s(): drawing area size is (%d,%d)\n",
	 "on_top_level_window_drawingarea1_expose_event", w, h);
      fflush (stdout);
    }

  /* save a copy of current size */
  drawing_area_xsize = w;
  drawing_area_ysize = h;
  /* check if there is node data to draw */
  if (validdata == 0)
    {
      /* white fill drawing area */
      cairo_set_source_rgb (crdraw, altr / 255.0, altg / 255.0, altb / 255.0);
      cairo_rectangle (crdraw, 0, 0, w, h);
      cairo_fill (crdraw);
      cairo_stroke (crdraw);
      cairo_destroy (crdraw);
      /* no data */
      return (FALSE);
    }

  cairo_set_source_rgb (crdraw, bgcr / 255.0, bgcg / 255.0, bgcb / 255.0);
  /* select whole screen to fill with background color */
  cairo_rectangle (crdraw, 0, 0, w, h);
  cairo_fill (crdraw);
  cairo_stroke (crdraw);
  /* use zoom slider drawing scale */
  cairo_scale (crdraw, zfactor, zfactor);
  on_top_level_window_drawingarea1_expose_event_nodes (crdraw);
  on_top_level_window_drawingarea1_expose_event_edges (crdraw);
  /* ready drawing */
  cairo_destroy (crdraw);
  return (FALSE);
}
#endif

#if GTK_HAVE_API_VERSION_3
/* redraw drawing area */
static gboolean
  on_top_level_window_drawingarea1_draw_event
  (GtkWidget * widget, cairo_t * crdraw, gpointer user_data)
{
  gint w = 0;			/* xsize of drawing area */
  gint h = 0;			/* ysize of drawing area */
  cairo_t *crp = NULL;
  /* this is a workaround for issue in cairo-lib 1.14.0 with gtk3,
   * cairo.c cairo_destroy() line 305 assert(), (with gtk2 no problem) */
  crp = cairo_reference (crdraw);
  if (widget)
    {
    }
  if (user_data)
    {
    }

  /* how large drawing area is */
  w = gtk_widget_get_allocated_width (drawingarea1);
  h = gtk_widget_get_allocated_height (drawingarea1);
  if (option_gdebug > 1)
    {
      printf
	("%s(): drawing area size is (%d,%d)\n",
	 "on_top_level_window_drawingarea1_draw_event", w, h);
      fflush (stdout);
    }

  /* save a copy of current size */
  drawing_area_xsize = w;
  drawing_area_ysize = h;
  /* check if there is node data to draw */
  if (validdata == 0)
    {
      /* white fill drawing area */
      cairo_set_source_rgb (crdraw, altr / 255.0, altg / 255.0, altb / 255.0);
      cairo_rectangle (crdraw, 0, 0, w, h);
      cairo_fill (crdraw);
      cairo_stroke (crdraw);
      cairo_destroy (crdraw);
      /* no data */
      return (FALSE);
    }


  cairo_set_source_rgb (crdraw, bgcr / 255.0, bgcg / 255.0, bgcb / 255.0);
  /* select whole screen to fill with background color */
  cairo_rectangle (crdraw, 0, 0, w, h);
  cairo_fill (crdraw);
  cairo_stroke (crdraw);
  /* use zoom slider drawing scale */
  cairo_scale (crdraw, zfactor, zfactor);
  on_top_level_window_drawingarea1_expose_event_nodes (crdraw);
  on_top_level_window_drawingarea1_expose_event_edges (crdraw);
  cairo_destroy (crp);
  return (FALSE);
}
#endif

/* update status text */
static void
update_status_text (char *text)
{

  /* update status text line */
  memset (charentry1buffer, 0, 64);
  /* */
  if (text)
    {
      snprintf (charentry1buffer, 64 - 1, "%s", text);
      if (option_gdebug > 1 || 1)
	{
	  printf ("%s(): %s\n", "update_status_text", text);
	  fflush (stdout);
	}
    }
  else
    {
      if (maingraph)
	{
	  snprintf (charentry1buffer, 64 - 1,
		    "%d nodes, %d edges, %d crossings",
		    maingraph->nnodes,
		    maingraph->nedges, maingraph->sugi_fcrossings);
	}
      else
	{
	  snprintf (charentry1buffer, 64 - 1, "no message");
	}
    }

  /* when running in console mode there is no entry1buffer */
  if (entry1buffer)
    {
      gtk_text_buffer_set_text (entry1buffer, charentry1buffer, -1);
      /* it is visible in the gui */
      /* only a re-draw needed */
      gtk_widget_queue_draw (drawingarea1);
    }

  return;
}

/* checkbox 1 is 'splines' */
static void
check1_toggle (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* toggle the splines option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      option_splines = 1;
    }
  else
    {
      option_splines = 0;
    }
  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* checkbox 2 is 'dummy's' draw dummy nodes */
static void
dummy1_toggle (GtkWidget * widget, gpointer window)
{
  if (widget)
    {
    }
  if (window)
    {
    }
  /* toggle the splines option */
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    {
      drawdummy = 1;
    }
  else
    {
      drawdummy = 0;
    }
  /* only a re-draw needed */
  gtk_widget_queue_draw (drawingarea1);
  return;
}

/* fit drawing in window */
static void
dofit (void)
{
  double xzscale = 1.0;
  double yzscale = 1.0;
  double newzscale = 1.0;
  double dval = 1.0;
  int val = 0;
  /* if no maxx, maxy is set */
  if (maxx == 0)
    {
      maxx = 1;
    }

  if (maxy == 0)
    {
      maxy = 1;
    }

  xzscale = (double) (1000 * drawing_area_xsize / maxx);
  yzscale = (double) (1000 * drawing_area_ysize / maxy);
  xzscale = xzscale / 1000.0;
  yzscale = yzscale / 1000.0;
  if ((xzscale - yzscale) > 0)
    {
      newzscale = yzscale;
    }
  else
    {
      newzscale = xzscale;
    }

  if (option_gdebug > 1)
    {
      printf
	("%s(): fit zoom to %f from xscale=%f and yscale=%f drawingarea=(%d,%d) maxy=(%d,%d)\n",
	 "dofit", newzscale, yzscale, xzscale,
	 drawing_area_xsize, drawing_area_ysize, maxx, maxy);
    }

  dval = log ((newzscale * (double) 50.0) / 3.0) - 50.0;
  dval = log (newzscale) / 3.0;
  dval = (dval * 50.0);
  dval = dval + 50.0;
  val = (int) dval;
  if (val < 0)
    {
      val = 0;
    }

  if (val > 100)
    {
      val = 100;
    }

  zfactor = exp ((double) (3 * (val - 50)) / (double) 50);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale1), val);
  if (option_gdebug > 1)
    {
      printf
	("%s(): new slider value is %d (dval=%f) zfactor=%f\n",
	 "dofit", val, dval, zfactor);
    }

  /* reset v xy min */
  vxmin = 0;
  vymin = 0;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjvscale2), 0);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adjhscale1), 0);
  return;
}


/*
 * update the (x,y) size of text in the nodes
 *
 * a GTK font pattern. These have the syntax
 * fontname [properties] [fontsize]
 *
 * where fontname is the family name,
 * properties is a list of property values separated by spaces,
 * and fontsize is the point size.
 * The properties that you may specify for GTK font patterns are as follows:
 * Slant properties: ‘Italic’ or ‘Oblique’. If omitted, the default (roman) slant is implied.
 * Weight properties: ‘Bold’, ‘Book’, ‘Light’, ‘Medium’, ‘Semi-bold’, or ‘Ultra-light’. If omitted, ‘Medium’ weight is implied.
 * Width properties: ‘Semi-Condensed’ or ‘Condensed’. If omitted, a default width is used.
 * Here are some examples of GTK font patterns:
 * Monospace 12
 * Monospace Bold Italic 12
 *
 * there are nodes with labels whicj is a piece of text on display
 * and nodes with a record type with multiple small text to layout
 * in multiple steps for the different fields.
 */
static void
static_maingtk_textsizes (void)
{
  cairo_surface_t *surface = NULL;
  cairo_t *crdraw = NULL;
  PangoLayout *layout = NULL;
  PangoFontDescription *desc = NULL;
  int w = 0;
  int h = 0;
  char buf[128];
  char *s = NULL;
  /* name of font to use, example "Sans" */
  const char *default_fontname = DEFAULT_FONTNAME;
  /* name of slant to use, example "Italic", "Oblique", "Roman" */
  const char *default_fontslant = DEFAULT_FONTSLANT;
  /* name of weight to use, example "Bold", "Book", "Light", "Medium", "Semi-bold", "Ultra-light" */
  const char *default_fontweight = DEFAULT_FONTWEIGHT;
  /* name of condensed to use, example "Semi-Condensed", "Condensed" */
  const char *default_fontcondensed = DEFAULT_FONTCONDENSED;
  /* font size to use, example "10", "18", "20" etc. */
  const char *default_fontsize = DEFAULT_FONTSIZE;
  /* */
  struct gml_nlist *nl;
  nl = maingraph->nodelist;
  while (nl)
    {
      if (nl->node->dummy)
	{
	  /* dummynode has no text */
	  nl->node->tx = 0;
	  nl->node->ty = 0;
	  nl->node->bbx = 0;
	  nl->node->bby = 0;
	}
      else
	{
	  /* node or edgelabel text */
	  if (nl->node->nlabel == NULL)
	    {
	      /* shouldnothappen */
	      nl->node->nlabel = uniqstr (" ");
	    }

	  /* if the labeltext is "" replace it with " " */
	  if (strlen (nl->node->nlabel) == 0)
	    {
	      /* to avoid cairo assert */
	      nl->node->nlabel = uniqstr (" ");
	    }

	  /* calculate the text area */
	  if (surface == NULL)
	    {
	      surface =
		cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 10, 10);
	    }

	  /* */
	  if (crdraw == NULL)
	    {
	      crdraw = cairo_create (surface);
	    }

	  /* */
	  layout = pango_cairo_create_layout (crdraw);
	  /* set the text to draw which is 0 terminated */
	  pango_layout_set_text (layout, nl->node->nlabel, -1);
	  /* create the fontname description */
	  memset (buf, 0, 128);
	  /* name of font to use */
	  default_fontname = uniqstr ((char *) DEFAULT_FONTNAME);
	  /* check if node has a specified font slant */
	  default_fontslant = uniqstr ((char *) DEFAULT_FONTSLANT);
	  /* check if node has a specified font weight */
	  default_fontweight = uniqstr ((char *) DEFAULT_FONTWEIGHT);
	  /* check if node has a specified font size */
	  default_fontsize = uniqstr ((char *) DEFAULT_FONTSIZE);
	  /* create the font name string */
	  snprintf (buf, (128 - 1), "%s %s %s %s %s",
		    default_fontname,
		    default_fontslant,
		    default_fontweight,
		    default_fontcondensed, default_fontsize);
	  /* copy string buffer */
	  s = uniqstr (buf);
	  /* */
	  desc = pango_font_description_from_string (s);
	  /* */
	  pango_layout_set_font_description (layout, desc);
	  /* */
	  pango_font_description_free (desc);
	  /* */
	  pango_cairo_update_layout (crdraw, layout);
	  /* */
	  w = 0;
	  h = 0;
	  /* */
	  pango_layout_get_size (layout, &w, &h);
	  /* */
	  g_object_unref (G_OBJECT (layout));
	  /* */
	  if (option_gdebug > 1)
	    {
	      printf ("%s(): size (%d,%d) for \"%s\"\n",
		      "static_maingtk_textsizes",
		      (w / PANGO_SCALE), (h / PANGO_SCALE), nl->node->nlabel);
	      fflush (stdout);
	    }

	  /* set in unode the text area size */
	  nl->node->tx = (w / PANGO_SCALE);
	  nl->node->ty = (h / PANGO_SCALE);
	  /* for a box */
	  nl->node->bbx = nl->node->tx + 4;
	  nl->node->bby = nl->node->ty + 4;
	}
      nl = nl->next;
    }


  /* clear the draw context etc */
  if (layout)
    {
      layout = NULL;
    }

  if (crdraw)
    {
      cairo_destroy (crdraw);
      crdraw = NULL;
    }

  if (surface)
    {
      cairo_surface_destroy (surface);
      surface = NULL;
    }

  return;
}

/* do final (x,y) of nodes/edges */
static void
finalxy (void)
{
  struct gml_nlist *nl;
  int hw = 0;
  int xoff = 0;
  int yoff = 0;
  int i = 0;
  /* x positioning */
  make_posnodes (maingraph);
  maxx = 0;
  xoff = 0;
  hw = 0;
  /* scan hor. to adjust the x positions. */
  for (i = 0; i < (widestnnodes + 1); i++)
    {
      /* x spacing between the hor. levels */
      xoff = xoff + xspacing;
      /* determine half-way of the xpos. */
      if (wpos[i] == 0)
	{
	  /* if only dummy nodes */
	  hw = xspacing / 2;
	}
      else
	{
	  hw = (wpos[i] / 2);
	}

      /* update with current x */
      hw = hw + xoff;
      nl = posnodes[i];
      /* scan the nodes at this x pos. */
      while (nl)
	{
	  /* center the node around the half-way */
	  nl->node->finx = (hw - (nl->node->bbx / 2));
	  if ((nl->node->finx + nl->node->bbx) > maxx)
	    {
	      maxx = (nl->node->finx + nl->node->bbx);
	    }

	  nl = nl->next;
	}

      /* x spacing between the hor. levels */
      xoff = xoff + xspacing;
      /* x to next pos. */
      xoff = xoff + wpos[i];
    }

  clear_posnodes (maingraph);
  /* y positioning */
  make_levelnodes (maingraph);
  maxy = 0;
  yoff = 0;
  hw = 0;
  /* scan vert. to adjust the y positions. */
  for (i = 0; i < (maxlevel + 1); i++)
    {
      /* y spacing between the vert. levels */
      yoff = yoff + yspacing;
      /* determine half-way of the ypos. */
      if (hpos[i] == 0)
	{
	  /* if only dummy nodes */
	  hw = yspacing / 2;
	}
      else
	{
	  hw = (hpos[i] / 2);
	}

      /* update with current y */
      hw = hw + yoff;
      nl = levelnodes[i];
      /* scan the nodes at this y pos. */
      while (nl)
	{
	  /* center the node around the half-way */
	  nl->node->finy = (hw - (nl->node->bby / 2));
	  /* update drawing max y pos used */
	  if ((nl->node->finy + nl->node->bby) > maxy)
	    {
	      maxy = (nl->node->finy + nl->node->bby);
	    }

	  /* give dummy nodes a vertical size of the level */
	  if (nl->node->dummy)
	    {
	      nl->node->bby = hpos[i];
	      /* if only dummy nodes at level, use spacing */
	      if (hpos[i] == 0)
		{
		  nl->node->bby = yspacing;
		}
	    }

	  nl = nl->next;
	}

      /* y spacing between the vert. levels */
      yoff = yoff + yspacing;
      /* y to next pos. */
      yoff = yoff + hpos[i];
    }

  clear_levelnodes (maingraph);
  return;
}

/* end */
