#
# /*
#  *  This program is free software: you can redistribute it and/or modify
#  *  it under the terms of the GNU General Public License as published by
#  *  the Free Software Foundation, either version 3 of the License, or
#  *  (at your option) any later version.
#  *
#  *  This program is distributed in the hope that it will be useful,
#  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  *  GNU General Public License for more details.
#  *
#  *  You should have received a copy of the GNU General Public License
#  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  *
#  * SPDX-License-Identifier: GPL-3.0+
#  * License-Filename: LICENSE
#  *
#  */
#
# clng llvm control flow graph:
# clang -emit-llvm hello.c -c -o main.bc
# opt -dot-cfg-only main.bc

bin_PROGRAMS=gml4gtk

gml4gtk_SOURCES = \
	gml_parser.c \
	gml_scanner.c \
	main.c \
	bubbling.c \
	sugi2.c \
	pos.c \
	hier.c \
	splay-tree.c \
	uniqstr.c \
	uniqnode.c


#
gml4gtk_CFLAGS = \
	@CFLAGS@ @PACKAGE_CFLAGS@ @WARNING_CFLAGS@ @GTK_CFLAGS@

#
gml4gtk_tolink = @GTK_LIBS@

#
gml4gtk$(EXEEXT): $(gml4gtk_SOURCES)
	$(CC) $(gml4gtk_CFLAGS) $(gml4gtk_SOURCES) -I. -I.. $(gml4gtk_tolink) -lm -o gml4gtk

#
indent:
	indent $(gml4gtk_SOURCES) main.h gml_parser.h gml_scanner.h bubbling.h sugi.h pos.h hier.h splay-tree.h uniqstr.h uniqnode.h

# clang -emit-llvm hello.c -c -o main.bc
# opt -dot-cfg-only main.bc
bc:
	clang -emit-llvm -c $(gml4gtk_CFLAGS) -I. -I.. sugi2.c -o sugi2.bc
	opt-7 -dot-cfg-only sugi2.bc

#
clean-generic:
	rm -v -f *.bc
	rm -v -f ./massif.out.*
	rm -v -f ./*.rhp.txt
	rm -v -f ./a.out
	rm -v -f *~
	rm -v -f ./*.so.1
	rm -v -f ./*.o
	rm -v -f ./*.a
	rm -v -f ./*.i
	rm -v -f ./*.s
	rm -v -f ./O
	rm -v -f ./OO
	rm -v -f ./O1
	rm -v -f ./O2
	rm -v -f ./O3
	rm -v -f ./dsmake.output
	rm -v -f ./dsmake.warnings
	rm -v -f ./dsmake.errors
	rm -v -f *.dot
	rm -v -f *.*r.*
	rm -v -f *.*t.*
	rm -v -f *.bc
	rm -v -f *.plist
	rm -v -f *.ps

# /* end */
